FROM sonarsource/sonar-scanner-cli

COPY requirements.txt requirements.txt

COPY reportgen.py reportgen.py
COPY wait.py wait.py

COPY pip-10.0.1-py2.py3-none-any.whl pip-10.0.1-py2.py3-none-any.whl.whl

RUN python pip-10.0.1-py2.py3-none-any.whl.whl/pip install --no-index pip-10.0.1-py2.py3-none-any.whl.whl --user

RUN ~/.local/bin/pip install setuptools --user && ~/.local/bin/pip install -r requirements.txt --user

ENTRYPOINT  [""]
